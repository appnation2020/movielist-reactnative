import {combineReducers} from 'redux';
import SeriesReducer from './reducer_movie';

const RootReducer = combineReducers({
    movieList : SeriesReducer 
});

export default RootReducer;