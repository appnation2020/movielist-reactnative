import {ADD_NEW_SERIES, DELETE_SERIES} from '../actions/ActionType';
const INITIAL_STATE = {
    seriesData: [],
  };
  
const SeriesReducer = (state = [] ,action) => {

    switch (action.type){
        case ADD_NEW_SERIES :
        return [
            ...state,
            {
                movieData : action.payload,
                id : action.id

            },

        ]
        // const {
        //     seriesData
        //   } = state;
    
        //   seriesData.push(action.payload);
    
        //   const newSeries = { seriesData };
        //   return newSeries;
          
        case DELETE_SERIES:
        return []
        default:
            return state;
    }
}

export default SeriesReducer;



// export default function(){
//     return[
//         {
//             "title" : 'Stranger Things',
//             "genres" : "Action",
//             "totalSeasons" : 2 , 
//             "description" : "it's hollywood awsome season.",
//             "language" : 'English'
//           }, 
//           {
//             "title" : 'Friends',
//             "genres" : "comady",
//             "totalSeasons" : 10 , 
//             "description" : "it's hollywood awsome season.",
//             "language" : 'English'
//           }
//     ]
// }