import React, { Component, PureComponent } from 'react';

import {
    StyleSheet,
    Text,
    View,
    TouchableOpacity,
    Alert,
    AsyncStorage
} from 'react-native';
import { TextInput, State } from 'react-native-gesture-handler';
import  ProgressBar  from '../utils/ProgressBar';

export default class Login extends Component {
    constructor() {
        super()
        this.state = {
            isLoading: false,
            _userName: '',
            _password: ''
        }
    }
    static navigationOptions = {
        title: 'Welcome Again',
        headerTitleStyle: {
            textAlign: 'center',
            flexGrow: 1,
        }
    }

    loginValidation = () => {
        var string = ' ';
        if (this.state._userName === '') {
            string = 'Enter valid email Address.';
        } else if (this.state._password === '') {
            string = 'Enter password.';
        }
        return string;
    }

    onButtonPress = () => {
        if (this.loginValidation() !== ' ') {
            console.log(this.loginValidation())
            Alert.alert("Enter valid username and password.")
        } else {
            this.loginRequest();
        }
    }

    loginRequest() {
        this.setState({ isLoading: true });
        let data = {
            method: 'POST',
            body: JSON.stringify({
                "User": {
                    'email': this.state._userName,
                    "password": '9099sjb',
                    "type": "gridle",
                    "grant_type": "password"
                }
            }),
            headers: {
                'Accept': 'application/json',
                'Content-Type': 'application/json',
                'Client-Id': '56752a9e-1080-493a-af99-17ac3e1e7419'
            }
        }

        return fetch('https://app.gridle.io/api/v1/users/login', data)
            .then(response => response.json())
            .then((responseJson) => {
                console.log(responseJson);
                this.setState({ isLoading: false })
                this._storeData();
                // this.props.navigation.push('MovieList')
            })
            .catch((error) => {
                console.error(error);
            });
    }

    _storeData = async () => {
        try {
            console.log('setUserToaken')
            await AsyncStorage.setItem('userToken', 'abc');
            console.log(await AsyncStorage.getItem('userToken'))
            this.props.navigation.navigate('App');

        } catch (error) {
            console.log(await AsyncStorage.getItem('userToken'))
        }
    };


    render() {
        return (
            <View style={styles.container}>
                <TextInput style={styles.input}
                    placeholder='Enter User Name'
                    keyboardType='email-address'
                    onChangeText={(text) => this.setState({ _userName: text })}
                />
                <TextInput style={styles.input}
                    placeholder='Enter Password Name'
                    secureTextEntry={true}
                    onChangeText={(text) => this.setState({ _password: text })}
                />

                <TouchableOpacity style={styles.buttonStyle}
                    onPress={this.onButtonPress}>
                    <Text style={{ textAlign: 'center', color: '#ffffff' }}>Login</Text>
                </TouchableOpacity>

                {this.state.isLoading && <ProgressBar loading={true} />}
            </View>
        )
    };
}

const styles = StyleSheet.create({
    container: {
        backgroundColor: '#ffffff',
        flex: 1,
        justifyContent: 'center'
    },
    input: {
        borderColor: "#bec3c0",
        borderWidth: 0.8,
        borderRadius: 4,
        margin: 10,
    },
    buttonStyle: {
        marginTop: 4,
        borderWidth: 0.5,
        borderColor: '#bec3c0',
        borderRadius: 4,
        padding: 10,
        backgroundColor: '#68a0cf',
        margin: 10,

    },

})