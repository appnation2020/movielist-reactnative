import React, { Component, PureComponent } from 'react';
import {
    StyleSheet,
    Text,
    View,
    FlatList,
    TouchableOpacity,
    AsyncStorage,
    Alert,
    Image
} from 'react-native';

import Icon from 'react-native-vector-icons/Ionicons';
import images from '../assets/AssetsImage'
import movieData from '../data/MovieData';
import Swipeout from 'react-native-swipeout'
import { connect } from 'react-redux';

class FlatListItem extends PureComponent {
    constructor(props) {
        super(props);
        this.state = {
            activeRowKey: null,

        };
        console.log(props.item)
    }

    render() {
        console.log(this.props.item)
        const swipout = {
            autoClose: true,
            rowId: this.props.index,

            onOpen: (sectId, rowId, direction) => {
                console.log(this.props)
                this.setState({ activeRowKey: this.props.item.keyExtractor })
            },
            onClose: (sectId, rowId, direction) => {
                if (this.state.activeRowKey != null) {
                    this.setState({ activeRowKey: null })
                }
            },
            right: [
                {
                    onPress: () => {
                        Alert.alert("Alert",
                            'Are you sure you want to delete this?',
                            [
                                { text: 'No', style: 'cancle' },
                                {
                                    text: 'yes',
                                    onPress: () => {
                                        movieData.splice(this.props.index, 1);
                                    }
                                }
                            ],
                            { cancelable: true })
                    },
                    text: 'Delete',
                    type: 'delete'
                }
            ],
            sectionId: 1,

        }
        return (
            <Swipeout {...swipout}>
                <View style={{
                    flex: 1,
                    backgroundColor: this.props.index % 2 == 0 ? '#eefafa' : '#fcf2fc',
                    margin: 2,
                    borderColor: this.props.index % 2 == 0 ? '#b4d9b9' : '#e7aaa8',
                    borderWidth: 0.5
                }}>
                    <View style={{ padding: 4 }}>
                        <View style={styles.detailContainer}>
                            <View style={{ flex: 25 }}>
                                <Text style={styles.title}>Title</Text>
                            </View>
                            <View style={{ flex: 55 }}>
                                <Text style={styles.value}>: {this.props.item.movieData.title}</Text>
                            </View>
                            <View style={{ flex: 20 }}>
                                <TouchableOpacity style={{ height: 10, width: 10, color: '#000' }}
                                    onPress={this.onPressDelete}
                                    activeOpacity={1.0}>
                                    <Image
                                        style={{ flex: 1, resizeMode: 'contain', alignItems: 'center' }}
                                        source={images.ic_delete}
                                    />
                                </TouchableOpacity>
                            </View>
                        </View>
                        <View style={styles.detailContainer}>
                            <View style={{ flex: 25 }}>
                                <Text style={styles.title}>Genres</Text>
                            </View>
                            <View style={{ flex: 75 }}>
                                <Text style={styles.value}>: {this.props.item.movieData.genres}</Text>
                            </View>
                        </View>
                        <View style={styles.detailContainer}>
                            <View style={{ flex: 25 }}>
                                <Text style={styles.title}>Seasons</Text>
                            </View>
                            <View style={{ flex: 75 }}>
                                <Text style={styles.value}>: {this.props.item.movieData.totalSeasons}</Text>
                            </View>
                        </View>
                        <View style={styles.detailContainer}>
                            <View style={{ flex: 25 }}>
                                <Text style={styles.title}>Language</Text>
                            </View>
                            <View style={{ flex: 75 }}>
                                <Text style={styles.value}>: {this.props.item.movieData.language}</Text>
                            </View>
                        </View>
                    </View>

                    <View style={{
                        height: 0.5,
                        backgroundColor: "#bec3c0"
                    }} />

                </View>
            </Swipeout>

        )
    }
}
class MovieList extends Component {
    constructor(props) {
        super(props);
        this.state = {
            MovieListData: [],
        };
        console.log(props)
    }

    static navigationOptions = ({ navigation }) => {
        const { params = {} } = navigation.state;

        return {
            title: "SeriesList",
            headerTitleStyle: {
                textAlign: 'center',
                flexGrow: 1,
                alignSelf: 'center',
            },
            headerLeft: (
                <TouchableOpacity >
                    <Text></Text>
                </TouchableOpacity>
            ),
            headerRight: (
                <View style={{
                    marginEnd: 8,
                    padding: 4
                }}>
                    <TouchableOpacity
                        onPress={params.navigateToAdd}
                    >
                        <Icon name="md-add" size={25} color="#000000" />
                    </TouchableOpacity>
                </View>
            ),
        }
    };

    componentDidMount() {
        this.props.navigation.setParams({ navigateToAdd: this.navigateAddMovie });
    }

    navigateAddMovie = () => {
        this.props.navigation.push('AddMovie')
    }
    onSignOut = async () => {
        await AsyncStorage.clear();
        this.props.navigation.navigate('Auth');
    };
    render() {
        const { navigation } = this.props;

        // var item = navigation.getParam('mData');
        // if (item !== undefined) {
        //     movieData.push(item)
        // }
        // console.log(this.props.movieList.movieData.title)


        return (
            <View style={styles.container}>
                <FlatList data={this.props.movieList}
                    keyExtractor={item => this.props.movieList.id}
                    ListHeaderComponent={this.renderHeader}
                    extraData={this.state}
                    renderItem={({ item, index }) => {
                        return (
                            <FlatListItem item={item} index={index} />
                        );
                    }}
                >

                </FlatList>
                <TouchableOpacity style={styles.buttonStyle}
                    onPress={this.onSignOut}>
                    <Text style={{ textAlign: 'center', color: '#ffffff' }}>Logout</Text>
                </TouchableOpacity>
            </View>
        )
    };
}
const styles = StyleSheet.create({
    container: {
        flex: 1,
        backgroundColor: '#FFFFFF',
        marginEnd: 4,
        marginTop: 4,
        marginStart: 4
    },
    detailContainer: {
        flex: 1,
        flexDirection: 'row',
        padding: 2
    },
    title: {
        fontSize: 16,
        fontWeight: "bold",
    },
    value: {
        fontSize: 16,

    },
    buttonStyle: {
        marginTop: 4,
        marginBottom: 4,
        borderWidth: 0.5,
        borderColor: '#bec3c0',
        borderRadius: 4,
        padding: 10,
        backgroundColor: '#68a0cf',

    },

});

const mapStateToProps = (state) => {
    // const { seriesList } = state
    // return { seriesList }
    return { movieList: state.movieList }
};

export default connect(mapStateToProps, {})(MovieList);