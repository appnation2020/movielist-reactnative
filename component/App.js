import React, {Component} from 'react';


import { createStackNavigator, createAppContainer ,createSwitchNavigator } from "react-navigation";
import MovieList from './MovieList';
import Login from './Login'
import AuthLoadingScreen from './AuthLoadingScreen';
import AddSeriesScreen from './AddSeriesScreen';
import { Provider } from 'react-redux';
import { createStore } from 'redux';
import RootReducer from '../reducer/index'

const AuthStack = createStackNavigator({ 
  Login : {
    screen : Login,
  },
});

const RootStack = createStackNavigator(
  {
    MovieList: {
      screen: MovieList, 
    },

    AddMovie: {
      screen :AddSeriesScreen,
    }
  },

  { initialRootName: 'Login' }
)

// const AppContainer = createAppContainer(RootStack);
const AppContainer =  createAppContainer(createSwitchNavigator(
  {
    AuthLoading: AuthLoadingScreen,
    App: RootStack,
    Auth: AuthStack,
  },
  {
    initialRouteName: 'AuthLoading',
  }
));
const store = createStore(RootReducer);
console.log("store" + store)

export default class App extends Component {
  render() {
    return (
      <Provider store={store}>
        <AppContainer />
      </Provider>
    )
  }
}

