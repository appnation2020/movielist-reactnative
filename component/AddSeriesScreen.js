import React, { Component } from 'react';
import {
  StyleSheet,
  Text,
  View,
  TextInput,
  Button,
  Picker
} from 'react-native';
import { TouchableOpacity, ScrollView } from 'react-native-gesture-handler';
import {AddNewSreies} from '../actions/index';
import {connect} from 'react-redux';
import { bindActionCreators } from 'redux'
import {ADD_NEW_SERIES} from '../actions/ActionType';

class HomeScreen extends Component {
  constructor(props) {
    super(props)
    this.state = {
      _title: '',
      _genres: "",
      _totalSeasons: 0,
      _description: "",
      _language: 'English'
    }
  }

  static navigationOptions = {

    title: "Add Series",
    headerTitleStyle: {
      textAlign: 'center',
      flexGrow: 1,
      alignSelf: 'center',
    },
    headerRight: (
      <TouchableOpacity>
        <Text> </Text>
      </TouchableOpacity>
    )
  } 


  buttonPressed = () => {
    var mData = {
      // id : this.getPrimaryKeyId(),
      title: this.state._title,
      genres: this.state._genres,
      totalSeasons: this.state._totalSeasons,
      description: this.state._description,
      language: this.state._language
    };
    this.props.navigation.navigate("MovieList", { mData });
    //  this.props.navigation.navigate("MovieList")
    this.props.AddNewSreies(mData);
    // this.addMovieInToList(mData);
  };

  // addMovieInToList = (mData) => {
  //     this.props.dispatch({type : 'ADD_NEW_SERIES',mData});
      
  // }


  render() {
    const { navigation } = this.props;
    return (
      <ScrollView style={styles.container} >
        <View >
          <Text style={styles.text}>Title</Text>

          <TextInput style={styles.input}
            placeholder='Enter title'
            onChangeText={(text) => this.setState(
              (priviousState) => {
                return {
                  _title: text
                }
              }
            )}
          />

          <Text style={styles.text}>Genres</Text>
          <View style={{ borderWidth: 0.5, borderColor: '#bec3c0', borderRadius: 5 }}>
            <Picker style={styles.input}
              textDecorationLine='underline'
              selectedValue={this.state._genres}
              onValueChange={(itemValue, itemIndex) => this.setState({ _genres: itemValue })}>
              <Picker.Item label='Action' value='Action' />
              <Picker.Item label='Comedy' value='Comedy' />
              <Picker.Item label='Horror' value='Horror' />
              <Picker.Item label='Romance' value='Romance' />
            </Picker>
          </View>

          <View style={styles.rowSection}>

            <View style={styles.rowContainer}
              paddingEnd={10}>
              <Text style={styles.text}>Seasions</Text>
              <TextInput style={styles.input}
                onChangeText={text => this.setState({ _totalSeasons: text })}
                keyboardType="numeric" />
            </View>

            <View style={styles.rowContainer}>
              <Text style={styles.text}>Language</Text>
              <TextInput style={styles.input}
                onChangeText={text => this.setState({ _language: text })}
              />
            </View>
          </View>

          <Text style={styles.text}>Description</Text>
          <TextInput
            style={[styles.input, styles.inputDescription]}
            multiline={true}
            onChangeText={text => this.setState({ _description: text })}
          />

          <View style={{ marginTop: 20 }}>
            <TouchableOpacity
              style={styles.buttonStyle}
              padding={10}
              onPress={this.buttonPressed}
               >
              <Text style={{ textAlign: 'center' ,color : '#ffffff' , fontSize : 18 }}>Send</Text>
            </TouchableOpacity>
          </View>
        </View>
      </ScrollView>
    );
  }
}


const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#ffffff',
    margin: 10,
    padding: 10

  },
  input: {
    borderColor: "#bec3c0",
    borderWidth: 0.8,
    borderRadius: 4,
  },
  text: {
    color: '#000000',
    fontSize: 16,
    margin: 6,
  },
  buttonStyle: {
    marginTop: 4,
    borderWidth: 0.5,
    borderColor: '#bec3c0',
    borderRadius: 4,
    padding: 10,
    backgroundColor: '#68a0cf',
  },
  seasonInput: {
    flex: 50,
  },
  rowSection: {
    flexDirection: 'row',

  },
  rowContainer: {
    flex: 1,
    backgroundColor: '#FFFFFF',
  },
  inputDescription: {
    height: 100,
    textAlignVertical: "top"
  }

});


const mapStateToProps = state =>{
    const { seriesList } = state
    return { seriesList }
}

const mapDispatchToProps = dispatch => (
  bindActionCreators({
    AddNewSreies,
  }, dispatch)
);

export default connect(mapStateToProps, mapDispatchToProps)(HomeScreen);
